#include "menu.hpp"

enum {
    WALK_OK = 0,
    WALK_BADPATTERN,
    WALK_BADOPEN,
};

vector<dirent> Menu::fillmenu(string dir)
{
    struct dirent *entry;
    vector<dirent>entries;
    regex_t reg;
    DIR *d; 

    chdir(dir.c_str());

    //const char *pattern=".*\\.cpp";
    // skip everything starting with . 
    const char *pattern="[^.]";
 
    if (regcomp(&reg, pattern, REG_EXTENDED | REG_NOSUB)) {
        cout << "Could not construct pattern" << endl;
        return entries;
    } 
    if (!(d = opendir("."))) {
        cout << "Could not open dir" << endl;
        return entries; 
    }

    while ( (entry = readdir(d)) )
        if (!regexec(&reg, entry->d_name, 0, NULL, 0))
            entries.push_back(*entry);

    closedir(d);
    regfree(&reg);

    return entries;
}

void Menu::clear(void)
{
    std::system("clear");
}

void Menu::add_dir_entries()
{
    Color::Modifier yellow(Color::FG_YELLOW);
    Color::Modifier def(Color::FG_DEFAULT);

    cout << yellow << "[b] " << def <<  "back"  << endl;
    cout << yellow << "[q] " << def <<  "quit"  << endl;
}

void Menu::add_file_entries()
{
    cout << endl << "-------------------- end of sample -------------" << endl;
    Color::Modifier yellow(Color::FG_YELLOW);
    Color::Modifier def(Color::FG_DEFAULT);

    cout << yellow << "[c] " << def <<  "code"  << endl;
    cout << yellow << "[e] " << def <<  "exec"  << endl;
}

void Menu::printmenu(vector<dirent>entries)
{
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier red(Color::FG_RED);

    for (int i = 0; i<entries.size(); i++) { 
        if (entries[i].d_type == DT_DIR) { 
            cout << red << "[" << i << "] " << def <<  entries[i].d_name << endl;
        } else { 
            cout << green << "[" << i << "] " << def <<  entries[i].d_name << endl;
        } 
    } 

    add_dir_entries();
}

bool Menu::is_numeric(string what)
{
    string::const_iterator it = what.begin();
    bool num = what.size() > 0;
    while (it != what.end()) {
       if (!std::isdigit(*it)) return false;
          it++;
    }

    return num;
}

void Menu::handleFile(dirent de)
{
<<<<<<< HEAD
    // pip install Pygments
=======
    clear();
>>>>>>> 7171527c8afde9141de7cb564d51b2c9bfe7c415
    string command = "pygmentize " + string(de.d_name);
    system(command.c_str());
    add_file_entries();
    add_dir_entries();
}

void Menu::runDirectory(string dirname)
{
    vector<dirent> entries = fillmenu(dirname);
    string line;
    dirent de;

    while (1) {
        printmenu(entries);
        getline(cin,line);
        if (line.compare("q") == 0) {
            exit(0);
        } else 
        if (line.compare("b") == 0) {
            clear();
            return;
        } else  {
            bool isNumber = is_numeric(line);
            if (!isNumber) { 
                clear();
                continue;
            } 
            int i = atoi(line.c_str());
            if (i >= entries.size()) { 
                clear();
                continue;
            } 

            de = entries[i];
            if (de.d_type == DT_DIR) { 
                clear();
                runDirectory(de.d_name);
                chdir("..");
                continue;
            } else { 
                clear();
                handleFile(de);
            } 
        } 
        getline(cin,line);
    } 
}

void Menu::run(void)
{
    string line;
    clear();
    runDirectory("src");
}

//#define STANDALONE 1
#ifdef STANDALONE
int main(int argc, char *argv[])
{
    Menu menu;
    menu.run();
}
#endif
