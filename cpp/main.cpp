#include <iostream>
#include "mod.h"
#include "menu.hpp"

int main(int argc, char *argv[]) { 
    Color::Modifier red(Color::FG_RED);
    Color::Modifier def(Color::FG_DEFAULT);
    std::cout << "Console output " << red << " demonstrated, " << def << " next..." << std::endl;

    Menu m;
    m.run();
}
