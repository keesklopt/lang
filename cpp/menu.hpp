#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <regex.h>
#include <iostream>
#include <vector>
#include <unistd.h>
#include "color.hpp"

using namespace std;

class Menu
{
public:
    void run(void);
    vector<dirent> fillmenu(string dir);
    void runDirectory(string dirname);
    void printmenu(vector<dirent>entries);
    void handleFile(dirent de);
    void add_dir_entries();
    void add_file_entries();
    bool is_numeric(string what);
    void clear(void);
};
