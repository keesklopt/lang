set modelines=1
set ts=4
set expandtab
"set makeprg=scons
set background=dark
"ok, this one is handy in python programming but the $ at the end
"interferes too much with copy-paste
"set list
"maybe we should always :
:retab
"set errorformat=%A%f:%l:\ %m,%-Z%p^,%-C%.%#
"ANT version of errorformat, but not needed when using ant -emacs
"set efm=\ %#[javac]\ %#%f:%l:%c:%*\\d:%*\\d:\ %t%[%^:]%#:%m,\%A\ %#[javac]\ %f:%l:\ %m,%-Z\ %#[javac]\ %p^,%-C%.%#
syntax on
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0

"set makeprg=mvn\ compile\ -q\ -f\ pom.xml
"set errorformat=[ERROR]\ %f:[%l%.%c]%m
