#!/usr/bin/env node

// 4 ways of making objects
// -----------------------------
// 1: Object initializer 

var obj = { property: 111, 
            2: "string"};

console.log(obj);
console.log("o: %o", obj);
console.log(obj.property);
console.log(obj['property']);
// but we need to do :
console.log(obj[2]);
// because this will fail : console.log(obj.2);

// you just created an object on-the-fly, there is no place to put any protoype code.

// 2: Constructor function

function FuncObj(a,b,c) {
    this.one=a;
    this.two=b;
    this.three=c;
}

var obj = new FuncObj(1,2,3);

console.log(obj);
console.log("o: ", obj);
console.log(obj.property);
console.log(obj['property']);

// still this is no 'struct' you can still add new members:
obj.property = 111;
console.log("o: ", obj);
console.log(obj.property);
console.log(obj['property']);

// plus this does support prototypes :
FuncObj.prototype.nogeen = 1;
var o2 = new FuncObj(3,2,1);
// also 1
console.log(o2.nogeen);
// now give it a new value 
o2.nogeen=2;
// now the property is present but different for both objects
console.log(obj);
console.log(obj.nogeen);
console.log(o2);
console.log(o2.nogeen);

// 3: using the Object.create method

var Animal = {
    type: "Invertebrates",
    displayType : function() {
        console.log(this.type);
    } 
}

// here is the creation
var animal1 = Object.create(Animal);

console.log(Animal);
console.log(animal1);

animal1.displayType();

c=1;
var arr = [c++,c++,,c++,"d"];
console.log(arr);
console.log(arr);

var b = new Boolean(false)
var bb = false

if (b) console.log('1');
if (bb) console.log('2');
if (b == true) console.log('3');
if (bb == true) console.log('4');


var o = { 
    een: 1,
    1: "een",
}

console.log(Object.keys(o));
console.log(Object.getOwnPropertyNames(o));

for (x in o) {
    console.log(x);
}
