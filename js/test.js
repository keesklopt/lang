#!/usr/bin/env node

function Car(make, model, year, owner)
{
    this.make=make;
    this.model=model;
    this.year=year;
    this.owner=owner;
}

var car1 = new Car("mitshubishi", "colt", 2007, "kees")

var Animal = { 
    type: "Invertebrates",
    displayType: function() {
        console.log(this.type);
    }
}

var a1 = Object.create(Animal);
a1.displayType();

var fish = Object.create(Animal);
fish.type = "Fishes"
fish.displayType();

Car.prototype.dinges = "oius";
Car.prototype.danges = "oius";

console.log(car1);
console.log("inf : ", ArrayBuffer);
console.log("obj : ", car1);
console.log("obj : ", Car.prototype);
console.log("obj : ", fish);
console.log(fish);
