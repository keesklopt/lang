#!/usr/bin/node

"use strict";

var fs = require("fs");

var Menu = function() {
}

Menu.prototype.run = function (text) {
    console.log("Running menu");
    var dir=".";

    var list = fs.readdirSync(dir)
    list.forEach(function(file) {
        var strs = file.split(".");
        if (strs[strs.length-1] == 'js') { 
            console.log(file);
        } 
    })
}

module.exports = Menu;

if (require.main == module) { 
     var menu = new Menu();
    menu.run();
} 

