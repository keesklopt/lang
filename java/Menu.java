import java.io.File;
import java.util.ArrayList;

class Menu
{
    ArrayList<String> contents;

    public void printmenu()
    {
        for (String file: contents) { 
            System.out.println(file);
        } 
    }
    
    public void fillmenu()
    {
        File dir = new File(".");
 
        contents = new ArrayList<String>();
        String[] list = dir.list();
        for (String file : list)
            if (file.endsWith(".java"))
                contents.add(file);
    }
    
    public void run()
    {
        fillmenu();
        printmenu();
    }
    
	public static void main(String[] arg) 
	{
        Menu menu = new Menu();
        menu.run();
	}
}
