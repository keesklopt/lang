#!/usr/bin/env node
var myVar = "global"; // Declare a global variable
function checkscope( ) {
     var myVar = "local";  // Declare a local variable
     console.log(myVar);
}
checkscope();
console.log(myVar);
