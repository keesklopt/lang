#!/usr/bin/env node
// with the shebang you can just call :
// ./main.js
// otherwise you should do :
// node main.js
console.log("Hello, world!");
// these are the argumenst, but the program name is not #1 so
console.log("Number of arguments : " + process.argv.length);
console.log("Argument 0 is : " + process.argv[0]);
console.log("Argument 1 is : " + process.argv[1]);
