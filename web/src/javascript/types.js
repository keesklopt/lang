#!/usr/bin/env node

/* types are not declared explicitely, and can change at every assignment */

var foo = 42;    // foo is now a Number, there is no separate int and float
console.log(foo);

foo = "bar"; // foo is now a String
console.log(foo);

foo = true;  // foo is now a Boolean
console.log(foo);
