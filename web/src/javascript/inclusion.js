#!/usr/bin/env node
// There is a difference between html where you use the script tags ...
// <script src="js/all.min.js"></script>

// ... and nodejs (commandline) where you can create modules:
var mod = require("./inclusion_mod.js");
mod.print();

// the file inclusion_mod.js contains this:
/*
module.exports = {
    print: function () {
        console.log("Module called");
    } 
};
*/

