#!/usr/bin/env node
// const makes a variable uncchangeable
const PI=3.14
console.log("PI : " + PI);

// BUT... not members : 
const MY_OBJECT = {"key": "value"};
console.log("OB : " , MY_OBJECT);
MY_OBJECT.key = "otherValue";
console.log("OB : " , MY_OBJECT);
