#!/usr/bin/env node
/* precedence rules : 
Value   Operator    Description Example
19      ( )         Expression grouping (3 + 4)
18      .           Member  person.name
18      []          Member  person["name"]
17      ()          Function call   myFunction()
17      new         Create  new Date()
16      ++          Postfix Increment   i++
16      --          Postfix Decrement   i--
15      ++          Prefix Increment    ++i
15      --          Prefix Decrement    --i
15       !          Logical not !(x==y)
15      typeof          Type    typeof x
14      *           Multiplication  10 * 5
14      /           Division    10 / 5
14      %           Modulo division 10 % 5
14      **          Exponentiation  10 ** 2
13      +           Addition    10 + 5
13      -           Subtraction 10 - 5
12      <<          Shift left  x << 2
12      >>          Shift right x >> 2
11      <           Less than   x < y 
11      <=          Less than or equal  x <= y
11      >           Greater than    x > y
11      >=          Greater than or equal   x >= y
10      ==          Equal   x == y
10      ===         Strict equal    x === y
10      !=          Unequal x != y
10      !==         Strict unequal  x !== y
6       &&          And x && y
5       ||          Or  x || y
3       =           Assignment  x = y
3       +=          Assignment  x += y
3       -=          Assignment  x -= y
3       *=          Assignment  x *= y
3       /=          Assignment  x /= y
*/
var x=9
var y=10

console.log("addition    : " + (x + y) );
console.log("inequality  : " + (x != y) );
console.log("left shift  : " + (x << y));
console.log("bitwise AND : " + (x & y));
console.log("modulo      : " + (x % y));
console.log("conditional : " + (x<y ? x : y));
