#!/usr/bin/env php
<?php
// with the shebang you can just call :
// ./main.php
// otherwise you should do :
// php main.php
print("Hello, world!\n");
print("Number of arguments: " . count($argv));
print("\nArgument 0 : " . $argv[0]);

?>
