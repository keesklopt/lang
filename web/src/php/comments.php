#!/usr/bin/env php
# shebang works but php starts after the tag, so these 
// comments just appear in the output
<?php
/* these do not */
// also a variety of comments work
# inside the php tags, also an empty program will 'run' fine
?>
