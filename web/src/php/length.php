#!/usr/bin/env php
<?php
$food = array('fruits' => array('orange', 'banana', 'apple'),
              'veggie' => array('carrot', 'collard', 'pea'));

// recursive count
echo count($food, COUNT_RECURSIVE); // output 8

echo "\xA"; // newline

// normal count
echo count($food); // output 2

?>
