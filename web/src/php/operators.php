#!/usr/bin/env php
<?php
/* precedence rules : 
Associativity       Operators                                   Additional Information
non-associative     clone new                                   clone and new
left                [                                           array()
right               **                                          arithmetic
right               ++ -- ~ (type) @                            types and increment/decrement
non-associative     instanceof                                  types
right               !                                           logical
left                * / %                                       arithmetic
left                + - .                                       arithmetic and string
left                << >>                                       bitwise
non-associative     < <= > >=                                   comparison
non-associative     == != === !== <> <=>                        comparison
left                &                                           bitwise and references
left                ^                                           bitwise
left                |                                           bitwise
left                &&                                          logical
left                ||                                          logical
right               ??                                          comparison
left                ? :                                         ternary
right               = += -= *= **= /= .= %= &= |= ^= <<= >>=    assignment
left                and                                         logical
left                xor                                         logical
left                or                                          logical
*/
$x = 10;
$y = 9;
print("addition    : " . ($x + $y));
print("\ninequality  : " . ($x != $y));
print("\nleft shift  : " . ($x << $y));
print("\nbitwise AND : " . ($x & $y));
print("\nmodulo      : " . ($x % $y));
print("\nconditional : " . ($x<$y ? $x : $y));
