#!/usr/bin/env php
<?php
// constants do NOT have to start with $ 
// it must be a letter or undescore though !
define("PI", 3.1415);
// now you can just use it : 
print("PI : " . PI);
// or use the constant function :
print("\nPI: " . constant("PI"));
?>
