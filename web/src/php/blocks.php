#!/usr/bin/env php
<?php
$a = 1;
$b = 2;

function func()
{
    global $a;

    $a = 100;
    $b = 200;
} 

func();
print($a . "\n");
print($b);
?>
