#!/usr/bin/env php
<?php
$a_bool = TRUE;   // a boolean
$a_str  = "foo";  // a string
$a_str2 = 'foo';  // a string
$an_int = 12;     // an integer

# also some functions handling types : 
echo gettype($a_bool) ."\n" ; // prints out:  boolean
echo gettype($a_str) . "\n";  // prints out:  string

// and testing a type : 
if (is_int($an_int)) {
    $an_int += 4;
}
echo $an_int;  // prints out:  string
?>
