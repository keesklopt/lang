#!/usr/bin/env php
<?php

# if inclusion_vars.php contains in it's body
# &lt;?php
# $color = 'green';
# $fruit = 'apple';
# ?&gt;

include 'inclusion_vars.php';
echo "A $color $fruit"; // A green apple
?>
