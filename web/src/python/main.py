#!/usr/bin/env python
# with the shebang you can just call :
# ./main.py
# otherwise you should do :
# python main.py
import sys
print("Hello, python!"); # and comments can follow a line

# command line arguments : (import sys needed)
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument 0:', sys.argv[0];
