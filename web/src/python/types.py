#!/usr/bin/env python

# types are not declared explicitely, and can change at every assignment

a = 1
s = 'abc'
b = a/2
a /= 2.0
t = 1==1    # boolean

print s;
print t;
print a;    # becomes a float
print b;    # stay integer and gets truncated
