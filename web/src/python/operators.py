#!/usr/bin/env python

# operator precendence

# Operator                                           Description
# lambda                                             Lambda expression
# if else                                            Conditional expression
# or                                                 Boolean OR
# and                                                Boolean AND
# not x                                              Boolean NOT
# in, not in, is, is not, <, <=, >, >=, <>, !=, ==   Comparisons, including membership tests and identity tests
# |                                                  Bitwise OR
# ^                                                  Bitwise XOR
# &                                                  Bitwise AND
# <<, >>                                             Shifts
# +, -                                               Addition and subtraction
# *, /, //, %                                        Multiplication, division, remainder [8]
# +x, -x, ~x                                         Positive, negative, bitwise NOT
# **                                                 Exponentiation [9]
# x[index], x[index:index], x(arguments...), x.attribute  Subscription, slicing, call, attribute reference
# (expressions...), [expressions...], {key: value...}, `expressions...`   

x=10;
y=9;
print ("addition    : " , (x+y));
print ("inequality  : " , (x != y));
print ("left shift  : " , (x << y));
print ("bitwise and : " , (x & y));
print ("logical or  : " , (x or y));
print ("modula      : " , (x % y));
