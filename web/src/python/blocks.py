#!/usr/bin/env python
import sys
# LEGB is Local, Enclosed, Global, Builtin
# Enclosed is outside a function, yet not on top level (global)

a_var = 'global value'

def outer():
    a_var = 'enclosed value'

    def inner():
        a_var = 'local value'
        print(globals()) # prints global namespace
        print(locals()) # prints local namespace
        print('so : a_var in inner():', 'a_var' in locals())
    
        print(a_var)

    inner()
    print(a_var)

outer()
print(a_var)
