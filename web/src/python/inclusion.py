#!/usr/bin/env python
# example of a module, it is important what you call this file
# this function below is enough for a module
def fib(n):    # write Fibonacci series up to n
    a, b = 0, 1
    while b < n:
        print b,
        a, b = b, a+b

# now for some usage: if you named the module fibo.py it can be imported with 
#import fibo
# or
#from fibo import fib

# complete example with this file (it's called inclusion.py)
# python
# import inclusion
# inclusion.fib(100)

# also if you want to use it as a standalone script as well add this :
if __name__ == "__main__":
    import sys
    fib(100)
