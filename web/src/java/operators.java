/* precedence rules : 
Operator Precedence Group                           Associativity   Operator Precedence
(), [], postfix ++, postfix --                      left            Highest
unary +, unary -, prefix ++, prefix --, ~, !        right   
(type), new                                         left    
*, /, %                                             left    
+, -                                                left    
<<, >>, >>>                                         left    
< ,<= , >, >=, instanceof       
==, !=      
&                                                   left    
^                                                   left    
|                                                   left    
&&                                                  left    
||                                                  left    
?:                                                  left    
=, +=, -=, *=, /=, %=, <<=, >>=, >>>=, &=, |=, ^=   right           lowest
*/
public class operators
{
    static int x = 10;
    static int y = 9;

    public static void main(String[] args) {
        System.out.println("addition    : " + (x + y) );
        System.out.println("inequality  : " + (x != y) );
        System.out.println("left shift  : " + (x << y));
        System.out.println("bitwise AND : " + (x & y));
        System.out.println("modulo      : " + (x % y));
        System.out.println("conditional : " + (x<y ? x : y));
    }
}
