// declaring your own package (example)
//package org.my.pkg;

// you import binary packages not actual source files like most languages
import java.io.*;
import java.lang.Math;

// including this class in another source file: (example)
//import org.my.pkg.inclusion;
// including all classes in another source file: (example)
//import org.my.pkg.*; 

public class inclusion {
    // class entry point is main in a public class, you can run main.class with this
    public static void main(String[] args) {
        System.out.println("Powerrr. !" + Math.pow(10,3));
    }
}
