// define a namespace/package for this file
package foo;
// use another namespace/package
import java.io.*;

public class namespaces {
    // this would need to be compiled like this :
    // javac -d . namespace.java
    // and the java command should be 
    // java foo.namespaces
    public static void main(String[] args) {
        // but neither is done by the web compile script, so this will not show:
        System.out.println("Bye foo.namespaces");
    }
}
