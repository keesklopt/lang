import java.io.*;

public class blocks {
    public static void main(String[] args) {
        // not global but outer scope
        int outer = 1;

        // also not, a blocks can appear like this :
        {
            int inner = 2;
            System.out.println("inner = " + inner);
            System.out.println("outer = " + outer);
        }

        // not a redefinion, inner is off the stack by now
        int inner = 3;
        System.out.println("inner = " + inner);
        System.out.println("outer = " + outer);
    }
}
