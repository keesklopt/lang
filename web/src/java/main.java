/* compilation : 
javac main.java 
    also note that the files has to be called main.java : same as the public class name
    the resulting main.class file can be run with 
java main 
*/

// single comments like this
// importing, with wilcard meaning : everything from java.io
import java.io.*;

public class main {
    // class entry point is main in a public class, you can run main.class with this
    public static void main(String[] args) {
        System.out.println("Hello, world!");
        System.out.println(args.length);
        // the program itself is not part of the arguments: will be empty
        System.out.println(args[0]);
    }
}
