/* example to get the size of an array and print it */

#include <array>
#include <iostream>
#include <string>
 
int main() 
{
    std::array<std::string, 2> fruit ={ "apples", "oranges" };
    std::cout << fruit.size();
    return 0;
}
