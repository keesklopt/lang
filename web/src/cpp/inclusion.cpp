// compile : g++ -stdc=c++11 main.cpp -o main
// run: ./main
#include <iostream>
int main(int argc, char* argv[]) 
{
    std::cout << "Hello, world!" << std::endl;
    std::cout << "Number of arguments" << argc << std::endl;
    std::cout << "Argument 0 : " << argv[0] << std::endl;
    return 0;
}

