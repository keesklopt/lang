#include <iostream>

// using the standard namespace
using namespace std;
// so you can use : endl instead of std::endl

// create namespaces like this:
namespace foo
{
    int value() { return 5; }
}

namespace bar
{
    const double pi = 3.1416;
    double value() { return 2*pi; }
}

int main(int argc, char* argv[]) 
    // using the same name from different namespaces :
    cout << foo::value() << endl;
    cout << bar::value() << std::endl;
    cout << bar::pi << '\n';
    return 0;
}
