/* example of native type declarations and usage */

#include <array>
#include <iostream>
#include <string>
 
int main() 
{
    short s=28973;
    int t=22;
    float f=2.33;
    char c='c';
    long long ll=2978628976120948730LL;

    // sizes of various types : 
    std::cout << "Size of char : " << sizeof(char) << std::endl;
    std::cout << "Size of int : " << sizeof(int) << std::endl;
    std::cout << "Size of short int : " << sizeof(short int) << std::endl;
    std::cout << "Size of long int : " << sizeof(long int) << std::endl;
    std::cout << "Size of float : " << sizeof(float) << std::endl;
    std::cout << "Size of double : " << sizeof(double) << std::endl;
    std::cout << "Size of wchar_t : " << sizeof(wchar_t) << std::endl;

    std::cout << "Size of long long : " << sizeof(long long) << std::endl;
    return 0;
}
