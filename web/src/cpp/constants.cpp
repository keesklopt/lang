#include <iostream>

// using define is still pretty constant
#define PI 3.1415

int main(int argc, char* argv[]) 
{
// however you probaly mean constant variables : 
    const double pi = 3.1415;

    //PI = 2.2; ->  error: lvalue required as left operand of assignment
    //pi = 2.2; ->  error: assignment of read-only variable

    std::cout << "Both give       : " << PI << std::endl;
    std::cout << "const behaviour : " << pi << std::endl;
    return 0;
}

