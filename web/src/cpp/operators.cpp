#include <iostream>
/* precedence rules : 
    Postfix             () [] -> . ++ - -                   Left to right 
    Unary               + - ! ~ ++ - - (type)* & sizeof     Right to left 
    Multiplicative      * / %                               Left to right 
    Additive            + -                                 Left to right 
    Shift               << >>                               Left to right 
    Relational          < <= > >=                           Left to right 
    Equality            == !=                               Left to right 
    Bitwise AND         &                                   Left to right 
    Bitwise XOR         ^                                   Left to right 
    Bitwise OR          |                                   Left to right 
    Logical AND         &&                                  Left to right 
    Logical OR          ||                                  Left to right 
    Conditional         ?:                                  Right to left 
    Assignment          = += -= *= /= %=>>= <<= &= ^= |=    Right to left 
    Comma               ,                                   Left to right 
*/
int main(int argc, char* argv[]) 
{
    int x = 10;
    int y = 9;
    std::cout << "addition    : " << (x + y) << std::endl;
    std::cout << "inequality  : " << (x != y) << std::endl;
    std::cout << "left shift  : " << (x << y) << std::endl;
    std::cout << "bitwise AND : " << (x & y) << std::endl;
    std::cout << "comma       : " << (x , y) << std::endl;
    std::cout << "modulo      : " << (x % y) << std::endl;
    std::cout << "conditional : " << (x<y ? x : y) << std::endl;
    std::cout << "address     : " << &x << std::endl;
    std::cout << "sizeof      : " << sizeof x << std::endl;
    return 0;
}
