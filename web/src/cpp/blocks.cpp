#include <iostream>
// outside of main, variables are global to the compilation unit : the file
int local=0;
int global=1;

// you can acces other global variable by using extern, no initializer 
extern int extglob;

/* blocks delimited by { } */
int main(int argc, char* argv[]) 
{
    // local scope variable, masks the one at global scope
    int local = 2;

    std::cout << "global is : " << global << std::endl;
    std::cout << "local is : " << local << std::endl;
    // however if you don't provide an object file with extglob defined, the linker will
    // fail at the next line ... so ... commented :
    // std::cout << "external is : " << extglob << std::endl;
    return 0;
}

