<?php

parse_str($_SERVER['QUERY_STRING'], $_GET);

// ok: i chose php because of it's ease of use under apache
// i think it deserves a spot as language as well then 

function remove_workdir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            self::remove_workdir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function tempdir($dir=false) {
    $tempfile=tempnam(sys_get_temp_dir(),'');
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}

function create_workdir()
{
    $dirname=tempdir(true);
    return $dirname;
}

function run_cpp($problem)
{
    $dirname = create_workdir();
    $syscommand = "cp src/cpp/" . $problem . ".cpp " . $dirname;
    //echo $syscommand;
    $result = system($syscommand);
    chdir($dirname);
    // at least g++4.8 needs this flag :
    $syscommand = "g++ -std=c++11 " . $problem . ".cpp";
    //echo $syscommand;
    $result = system($syscommand);
    $syscommand = "./a.out";
    $result = shell_exec($syscommand);
    remove_workdir($dirname);
    return $result;
}

function run_java($problem)
{
    $dirname = create_workdir();
    $syscommand = "cp src/java/" . $problem . ".java " . $dirname;
    //echo $syscommand;
    $result = system($syscommand);
    chdir($dirname);
    $syscommand = "javac " . $problem . ".java";
    //echo $syscommand;
    $result = system($syscommand);
    $syscommand = "java " . $problem;
    $result = shell_exec($syscommand);
    remove_workdir($dirname);
    return $result;
}

function run_js($problem)
{
    $dirname="src/javascript";
    //chdir($dirname);
    $syscommand = "./" . $dirname . "/" . $problem . ".js";
    //$syscommand = "ls";
    $result = shell_exec($syscommand);
    return $result;
}

function run_python($problem)
{
    $dirname="src/python";
    //chdir($dirname);
    $syscommand = "./" . $dirname . "/" . $problem . ".py";
    //$syscommand = "ls";
    $result = shell_exec($syscommand);
    return $result;
}

function run_php($problem)
{
    $dirname="src/php";
    //chdir($dirname);
    $syscommand = "./" . $dirname . "/" . $problem . ".php";
    //$syscommand = "ls";
    $result = shell_exec($syscommand);
    return $result;
}

    $language=$_GET["l"];
    $problem=$_GET["p"];
    //print_r($_GET);
    //print($language);
    //print($problem);

    switch ($language) { 
        case "cpp":
            $output = run_cpp($problem);
            echo $output;
        break;
        case "javascript":
            $output = run_js($problem);
            echo $output;
        break;
        case "python":
            $output = run_python($problem);
            echo $output;
        break;
        case "java":
            $output = run_java($problem);
            echo $output;
        break;
        case "php":
            $output = run_php($problem);
            echo $output;
        break;
        default:
            echo "Sample could not be compiled/run";
        break;
    } 
?>
