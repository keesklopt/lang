1. Introduction 

I would like the ultimate program to be made here for all languages i know.
It should simply be runnable, and display a menu of parts. 

One option would be to implement a directory scanner and a system exec function.
But this would mean that items are not really reusable, like Color, Input etc...

However, since these examples are meant to be cut-and-paste useable, i think we should construct the menu from the source lists, and run the samples with exec, and list them with some highlighter.

2. Languages

The languages supported : note that c is missing since i know that already ;)
And a description... later...

C++    : 
Java   : 
Python :
NodeJs :

3. Conventions

To be done.

4. Parts 

These are the parts that will be implemented, this list will grow in time, but the first parts have to be present and
implementable by all languages.

4.1 Main 

The main entry, with a menu to choose from. Let's just call it main in all cases.

4.1.1 Print messages in color

Just mention what language, that it is main, and put some color in.

4.1.2 Read subdirectory for parts.

Let the menu structure follow the directory structure. Also expect menu choice to be entered after return.
If we need ad getkey functionality let it be a subfunction among the other examples.

some standard keys (always mention what they mean as well) 

[00] do-this 
[c] show code
[r] run code
[q] quit (really exit)
[b] back (up directory) 
